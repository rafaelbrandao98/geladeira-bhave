import 'package:flutter/material.dart';
import 'package:geladeira_bHave/app/controller/home_controller.dart';
import 'package:get/get.dart';

import 'menu_tile.dart';

class DrawerCusom extends StatelessWidget {
  final HomeController homeController = Get.find();

  @override
  Widget build(BuildContext context) {
    Widget build() => Container(
          decoration: BoxDecoration(color: Colors.white),
        );
    return Drawer(
      child: Stack(
        children: [
          build(),
          ListView(
            shrinkWrap: true,
            children: [
              Row(
                children: [
                  Flexible(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0, left: 20),
                        child: CircleAvatar(
                            radius: 30,
                            backgroundColor: Colors.cyan,
                            child: new Image.asset(
                              'assets/bbh.png',
                            )),
                      )),
                  Flexible(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0, left: 12),
                        child: Column(
                          children: [
                            Text(
                              "Geladeira bHave",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Theme.of(context).primaryColor),
                            ),
                          ],
                        ),
                      )),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Divider(
                color: Colors.grey,
                indent: 10,
                endIndent: 10,
                thickness: 0.2,
                height: 3,
              ),
              Column(
                children: [
                  DrawerTile(
                    icon: 'geladeira',
                    text: 'Minha geladeira',
                    controller: homeController.pageController,
                    page: 0,
                  ),
                  Divider(),
                  DrawerTile(
                    icon: 'historico',
                    text: "Histórico",
                    controller: homeController.pageController,
                    page: 1,
                  ),
                  Divider(),
                  DrawerTile(
                    icon: 'top5',
                    text: "Top 5 mais consumidos",
                    controller: homeController.pageController,
                    page: 2,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
