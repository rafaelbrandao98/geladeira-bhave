import 'package:flutter/material.dart';
import 'package:geladeira_bHave/app/controller/home_controller.dart';
import 'package:geladeira_bHave/app/ui/geladeira.dart';
import 'package:geladeira_bHave/app/ui/historico.dart';
import 'package:geladeira_bHave/app/ui/top5.dart';
import 'package:get/get.dart';

class Home extends StatelessWidget {
  final HomeController controller = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: controller.pageController,
      children: [Geladeira(), Historico(), TopFive()],
    );
  }
}
